package com.hendisantika.springbootversioningrestfulapi.controller;

import com.hendisantika.springbootversioningrestfulapi.model.CustomerVersioning1;
import com.hendisantika.springbootversioningrestfulapi.model.CustomerVersioning2;
import com.hendisantika.springbootversioningrestfulapi.model.Email;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-versioning-restful-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-17
 * Time: 21:27
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    //Uri Versioning
    @GetMapping({"/v1.0", "/v1.1"})
    public CustomerVersioning1 getCustomerV1ByUriVersioning() {
        return generateDataCustomerV1();
    }

    @GetMapping({"/v2.0", "/v2.1"})
    public CustomerVersioning2 getCustomerV2ByUriVersioning() {
        return generateDataCustomerV2();
    }

    //Request Parameter Versioning
    @GetMapping(value = "/param", params = "v=1")
    public CustomerVersioning1 getCustomerV1ByRequestParameterVersioning() {
        return generateDataCustomerV1();
    }

    @GetMapping(value = "/param", params = "v=2")
    public CustomerVersioning2 getCustomerV2ByRequestParameterVersioning() {
        return generateDataCustomerV2();
    }

    //Custom HTTP headers
    @GetMapping(value = "/customHeader", headers = "X-API-V=1")
    public CustomerVersioning1 getCustomerV1ByCustomHTTPHeaders() {
        return generateDataCustomerV1();
    }

    @GetMapping(value = "/customHeader", headers = "X-API-V=2")
    public CustomerVersioning2 getCustomerV2ByCustomHTTPHeaders() {
        return generateDataCustomerV2();
    }

    //Media Type Versioning
    @GetMapping(value = "/produces", produces = "application/vnd.naruto.app-v1+json")
    public CustomerVersioning1 getCustomerV1ByMediaTypeVersioning() {
        return generateDataCustomerV1();
    }

    @GetMapping(value = "/produces", produces = "application/vnd.naruto.app-v2+json")
    public CustomerVersioning2 getCustomerV2ByMediaTypeVersioning() {
        return generateDataCustomerV2();
    }

    //Generate Data
    private CustomerVersioning1 generateDataCustomerV1() {
        CustomerVersioning1 customer = new CustomerVersioning1();
        customer.setFullName("Test 1");
        customer.setEmail("naruto@konohagakure.com");

        return customer;
    }

    private CustomerVersioning2 generateDataCustomerV2() {
        CustomerVersioning2 customer = new CustomerVersioning2();
        customer.setFullName("Test 2");

        Email email = new Email();
        email.setEmailFirst("sasuke@konohagakure.com");
        email.setEmailSecond("sakura@konohagakure.com");

        customer.setEmail(email);

        return customer;
    }
}