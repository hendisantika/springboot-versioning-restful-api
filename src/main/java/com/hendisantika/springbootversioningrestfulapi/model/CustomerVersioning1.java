package com.hendisantika.springbootversioningrestfulapi.model;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-versioning-restful-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-17
 * Time: 21:25
 */
public class CustomerVersioning1 {
    private String fullName;
    private String email;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
