package com.hendisantika.springbootversioningrestfulapi.model;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-versioning-restful-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-17
 * Time: 21:26
 */
public class CustomerVersioning2 {
    private String fullName;
    private Email email;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }
}
