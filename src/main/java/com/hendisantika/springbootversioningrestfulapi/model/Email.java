package com.hendisantika.springbootversioningrestfulapi.model;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-versioning-restful-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-17
 * Time: 21:26
 */
public class Email {
    private String emailFirst;
    private String emailSecond;

    public String getEmailFirst() {
        return emailFirst;
    }

    public void setEmailFirst(String emailFirst) {
        this.emailFirst = emailFirst;
    }

    public String getEmailSecond() {
        return emailSecond;
    }

    public void setEmailSecond(String emailSecond) {
        this.emailSecond = emailSecond;
    }
}
