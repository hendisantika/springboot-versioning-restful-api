package com.hendisantika.springbootversioningrestfulapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootVersioningRestfulApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootVersioningRestfulApiApplication.class, args);
    }

}
