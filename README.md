# Spring Boot Versioning RESTful API

## Thing to do
- Clone Repository:
```
git clone https://gitlab.com/hendisantika/springboot-versioning-restful-api.git
```

- Go to the folder: `cd springboot-versioning-restful-api`
- Run the application: `mvn clean spring-boot:run`

## Demo
We will use the Postman tool to test the RESTFul Web Services Versioning.
## 1. Uri Versioning Examples
Request Method: GET

Enter Request URL: http://localhost:8080/customer/v1.0

![Versioning Examples](img/versioning1.png)

Test with [HTTPie](https://httpie.org/)
Request : 
```
http http://localhost:8080/customer/v1.0
```

Response:
```
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Date: Mon, 18 Mar 2019 13:47:46 GMT
Transfer-Encoding: chunked

{
    "email": "naruto@konohagakure.com",
    "fullName": "Test 1"
}
```

Request Method: GET
Enter Request URL: http://localhost:8080/customer/v2.0

![Versioning Examples](img/versioning1.png)

Request : 
```
http http://localhost:8080/customer/v2.0
```

Response:
```
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Date: Mon, 18 Mar 2019 13:51:36 GMT
Transfer-Encoding: chunked

{
    "email": {
        "emailFirst": "sasuke@konohagakure.com",
        "emailSecond": "sakura@konohagakure.com"
    },
    "fullName": "Test 2"
}
```
##  2. Request Parameter Versioning Example
Request Method: GET
Enter Request URL: http://localhost:8080/customer/param?v=1

[Request Parameter Versioning Example](img/param1.png)

Request : 
```
http http://localhost:8080/customer/param?v=1
```

Response:
```
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Date: Mon, 18 Mar 2019 13:54:39 GMT
Transfer-Encoding: chunked

{
    "email": "naruto@konohagakure.com",
    "fullName": "Test 1"
}
```

Request Method: GET
Enter Request URL: http://localhost:8080/customer/param?v=2

[Request Parameter Versioning Example](img/param2.png)

Request : 
```
http http://localhost:8080/customer/param?v=2
```

Response:
```
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Date: Mon, 18 Mar 2019 13:55:46 GMT
Transfer-Encoding: chunked

{
    "email": {
        "emailFirst": "sasuke@konohagakure.com",
        "emailSecond": "sakura@konohagakure.com"
    },
    "fullName": "Test 2"
}
```

## 3. Custom Request Header Versioning Example
Request Method: GET
Enter Request URL: http://localhost:8080/customer/customHeader
headers=[X-API-V=1]

    HeaderName: X-API-V
    HeaderValue: 1

[Custom Request Header Versioning Example](img/header1.png)

Request:
```
http http://localhost:8080/customer/customHeader X-API-V:1
```

Response:
```
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Date: Mon, 18 Mar 2019 14:14:15 GMT
Transfer-Encoding: chunked

{
    "email": "naruto@konohagakure.com",
    "fullName": "Test 1"
}
```

[Custom Request Header Versioning Example](img/header2.png)

Request:
```
http http://localhost:8080/customer/customHeader X-API-V:2
```

Response:
```
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Date: Mon, 18 Mar 2019 14:16:11 GMT
Transfer-Encoding: chunked

{
    "email": {
        "emailFirst": "sasuke@konohagakure.com",
        "emailSecond": "sakura@konohagakure.com"
    },
    "fullName": "Test 2"
}
```

## 4. Media Type Versioning Example
Request Method: GET

Enter Request URL: http://localhost:8080/customer/produces
headers=[Accept=application/vnd.naruto.app-v1+json]

    HeaderName: Accept
    HeaderValue: application/vnd.naruto.app-v1+json

[Media Type Versioning Example](img/media1.png)

Request:
```
http http://localhost:8080/customer/produces Accept:application/vnd.naruto.app-v1+json
```

Response:
```
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Date: Mon, 18 Mar 2019 14:16:11 GMT
Transfer-Encoding: chunked
HTTP/1.1 200 
Content-Type: application/vnd.naruto.app-v1+json;charset=UTF-8
Date: Mon, 18 Mar 2019 14:19:30 GMT
Transfer-Encoding: chunked

{
    "email": "naruto@konohagakure.com",
    "fullName": "Test 1"
}
```

Request Method: GET
Enter Request URL: http://localhost:8080/customer/produces
headers=[Accept=application/vnd.naruto.app-v2+json]

    HeaderName: Accept
    HeaderValue: application/vnd.jackrutorial.app-v2+json

[Media Type Versioning Example](img/media2.png)

Request:
```
http http://localhost:8080/customer/produces Accept:application/vnd.naruto.app-v2+json
```

Response:
```
HTTP/1.1 200 
Content-Type: application/vnd.naruto.app-v2+json;charset=UTF-8
Date: Mon, 18 Mar 2019 14:20:59 GMT
Transfer-Encoding: chunked

{
    "email": {
        "emailFirst": "sasuke@konohagakure.com",
        "emailSecond": "sakura@konohagakure.com"
    },
    "fullName": "Test 2"
}
```